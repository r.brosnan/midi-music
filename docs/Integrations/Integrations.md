# Integrations

The application will need to get the following kinds of data from third-party sources:

- Music data from [MusicBrainz API](https://musicbrainz.org/doc/MusicBrainz_API)
- Wikipedia data from [Wikipedia API](https://pypi.org/project/wikipedia/#description)
