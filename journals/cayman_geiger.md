## 14NOV2023
1. Added YML File
2. Made HomePage Carousel And Nav Bar Media Queries For Hamburger Menu
3. Made Detail Page For Artists

    Today I got our YML file working and our home page image carousel working.
    I also got the detail page looking nice!


## 15NOV2023
1. Added The Nav Bar
2. Added The Search Functionality
3. Updated The Detail Page, Fixed Grids

    Today I got our detail page super dynamic, I also got the search bar working now I just need to do the fetching on it. I also got the nav bar working with a hambuger drop down.


## 16NOV2023
1. Finished Search Page And CSS for it
2. Added The Search Functionality To Fetch
3. Updated The Detail Page Again And Fixed Grids More

    Today I got the search working by using a searchcontext file to pass props to the search page from the nav.
    I also used :id on detail page and useParams to access the URL id to look up the artist


## 17NOV2023
1. Changed Detail Page Photos
2. Added Sign In Modal To Home Page

    Today I changed the detail page and added modals to everything


## 18NOV2023
1. Changed Detail Page For Albums To Look Better
2. Added Reviews To Album Page

    Today I changed the detail page and I reworked all of the CSS and JSX from scratch.
    I added the review section below as well


## 19NOV2023
1. Added review modal to review so user can click write a review
2. Added a context to track Mbid's

    Today I changed the detail page more to include a modal and a new context for data


## 20NOV2023
1. I made pages work with location query to pull data with navigation for use in another file
2. Made it so the search bar does not have errors and change css around the website to look better

    Today I focused on making things look better in general and learned how to work with useLocation.
    I succesfully used the URLParams and have it working.


## 21NOV2023
1. I made a full data context that uses local storage to save certain data to be used around the website and clears it on other pages
2. Changed all pages to use the local storage and delete or add things as needed to it

    Today I got local storage working and implemented, it properly saves things to local storage from the context and whenever you go to a page that does not need certain data I am doing cleanup and clearing the cache unless that page needs certain data than I exclude it.

## 22NOV2023
1. Today, I focused on enhancing the code's efficiency. I successfully improved the speed of the code, which has significantly boosted the website's performance. Additionally, I implemented new CSS styles, giving the website a fresh and more engaging look. This combination of speed and style has notably enhanced user experience.

## 23NOV2023
1. I integrated search effects into the website, adding an interactive element to the user experience. Furthermore, I continued my efforts to optimize the website's speed, ensuring a smoother and faster browsing experience for users.

## 24NOV2023
1. Today was dedicated to the favorites page. I added detailed elements to enrich user interaction. I also increased the loading speed of the detail pages, ensuring a more efficient browsing experience.

## 25NOV2023
1. Significant changes were made to the search page. I implemented a retry mechanism for failed fetch requests, enhancing the site's reliability. Additionally, a new context was added, improving the page's functionality and user interaction.

## 26NOV2023
1. I enhanced the data context to better utilize local storage, ensuring a more efficient data handling process. This improvement was also integrated across our files, promoting consistency in data management.

## 27NOV2023
1. Focused on optimizing data fetching, I transformed each fetch request into a useEffect and restructured functions for improved speed. This has resulted in a more responsive and efficient data retrieval process.

## 28NOV2023
1. Today's efforts were directed towards the user interface and experience. I added new CSS and HTML elements to various pages, significantly enhancing their appearance and usability.

## 29NOV2023
1. I made the local storage more efficient and addressed issues with photo searches. In cases of failed searches, the system now effectively handles and displays photos, ensuring a seamless user experience.

## 30NOV2023
1. Implemented a fail-safe mechanism for broken photo links, including a default photo option. This ensures that the website maintains a consistent and professional appearance, even in case of broken links.

## 1DEC2023
1. I revamped the search page to be more dynamic, contributing to a significant overall speed increase of the website. This has made the website more responsive and user-friendly.

## 2DEC2023
1. Today, I added loading icons to the search page, enhancing visual feedback during data retrieval. I also customized the TypeScript for React JWT to better suit our specific requirements, ensuring a more tailored user experience.

## 4DEC2023
1. Added hover effects to search photos, adding an interactive element to the website. Also, I improved the login and sign-up processes, making them more user-friendly and efficient.

## 5DEC2023
1. Implemented an effect that makes a cd roll out of the album on search page photos, extending this feature throughout the website. This not only enhances the visual appeal but also improves the user's interaction with the site.
