## 13 Nov

Created a page for Album Picture

## 14 Nov
Title and Picture are now updated dynamically

## 15 Nov
Updated Title CSS and fixed docker issue

## 16 Nov
Created a concert page using concert API 10 (only 10 calls per day)

## 17 Nov
Continued working on concert page, fixed the calls and added styling.

## 20 Nov

Fixed the bug on Album Pictures, added option to download picture in 250px, 500px and 1200px

## 21 Nov

Fixed docker issues
## 22 Nov

Worked on finishing Album page and tested it with different artists

## 27 Nov

Worked on Album page CSS

## 28 Nov

Started to integrate the Album pictures in the Album details page

## 29 Nov

Worked with Dante to keep integrating Album pictures

## 30 Nov

Added a search bar functionally in concert page to look for artists upcoming concert in USA

## 1 Dec

Added a ticket button in concert page so you can buy tickets for the artist user is searching

## 4 Dec

Added a filter option where the user can now look for upcoming concerts in 14 different countries

## 5 Dec

Worked on a bug where it was showing concerts for Spain even when filtered for different countries
