## November 6th, 2023 - November 10th, 2023
1. Worked and helped to create wire frame
2. Helped to come up with the proper models
3. Gave an Idea regarding the look of the home screen before user login


## November 13th 2023 - November 17th
1. Absent for loss of a family member


## November 20th, 2023 - November 29th, 2023
1. Got caught up from being absent


## December 4th, 2023 - December 8th, 2023
1. Created a Unit test for the creating a favorite album
2. Helped to deploy application


## December 11th, 2023
1. Ran through the Project Initialization steps in the READ.ME file.
