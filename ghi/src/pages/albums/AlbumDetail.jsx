import React, { Suspense, useState, useEffect, useCallback } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./AlbumDetail.css"
import "../artists/ArtistDetailFinal.css"
import useWindowSize from "../../extrafunctions/helpers/WindowSize";
import { usePhotoModal } from '../../modals/photos/usePhoto';
import { useData } from '../../context/datacontext/useData';
const ReviewForm = React.lazy(() => import("../../modals/reviews/create/ReviewingForm"));
const AverageRating = React.lazy(() => import("../../modals/reviews/display/AverageReview"));
const Ratings = React.lazy(() => import("../../modals/reviews/display/Reviews"));



function AlbumDetail() {
  const { width } = useWindowSize();
  const [showReviewModel, setShowReviewModel] = useState(false);
  const [isPhotoLoaded, setIsPhotoLoaded] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [activeTab, setActiveTab] = useState('tracks');
  const [isExpanded, setIsExpanded] = useState(false);
  const [photos, setPhotos] = useState([]);
  const [wikiAlbum, setWikiAlbum] = useState('');
  const [tracks, setTracks] = useState([]);
  const { openPhotoModal } = usePhotoModal();
  const { releaseGroupMbid, artistMbid, artistName, albumName } = useData();
  const [newReviewSubmitted, setNewReviewSubmitted] = useState(false);
  const [refetching, setRefetching] = useState(false);
  const defaultImage = "https://st3.depositphotos.com/23594922/31822/v/450/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"
  const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));


  useEffect(() => {
    setPhotos([]);
    setTracks([]);
  }, []);



  const fetchWithRetry = useCallback(async (url, options, retries = 4, delayDuration = 2000) => {
    await delay(700)
    try {
      const response = await fetch(url, options);
      if (!response.ok) {
        throw new Error(`Request failed with status ${response.status}`);
      }
      return await response.json();
    } catch (error) {
      if (retries > 0) {
        setRefetching(true)
        await delay(delayDuration);
        return await fetchWithRetry(url, options, retries - 1, delayDuration);
      } else {
        throw new Error(`Max retries reached for ${url}`);
      }
    }
  }, []);



  const loadPhotos = useCallback(async (releaseMbid) => {
      await delay(200)
      const url = (`https://coverartarchive.org/release/${releaseMbid}`);
      const data = await fetchWithRetry(url, {});

      if (data) {
        const AlbumPhotos = data.images;
        setPhotos(AlbumPhotos);
      }
  }, [setPhotos, fetchWithRetry]);


  const loadTracks = useCallback(async (releaseMbid) => {
      await delay(200)
      const url = (
        `https://musicbrainz.org/ws/2/release/${releaseMbid}?inc=recordings&fmt=json`
      );
      const data = await fetchWithRetry(url, {});

      if (data) {
        const trackTitles = data.media[0].tracks.map((track) => track.title);
        setTracks(trackTitles);
      }
  }, [setTracks, fetchWithRetry]);


  useEffect(() => {
    async function loadReleaseMbid() {
      const limit = 100;
      let offset = 0;
      let allReleases = [];
      let found = false;

      if (!albumName) {
        return;
      }

      while (!found) {
        try {
          await delay(200)
          const url = `https://musicbrainz.org/ws/2/release?artist=${artistMbid}&limit=${limit}&offset=${offset}&fmt=json`;
          const data = await fetchWithRetry(url, {});
          allReleases = allReleases.concat(data.releases);

          if (allReleases.length > 0) {
            const archive = allReleases.filter(
              (item) => item["cover-art-archive"].count >= 1 &&
                item.title.toLowerCase() === albumName.toLowerCase()
            );
            const archiveId = archive.reduce((currentHighest, item) => {
              return item["cover-art-archive"].count > currentHighest["cover-art-archive"].count
                ? item
                : currentHighest;
            }, archive[0]);

            if (archiveId && archiveId.id) {
              loadPhotos(archiveId.id);
              loadTracks(archiveId.id);
              found = true;
            }

            if (data.releases.length < limit) {
              found = true;
            } else {
              offset += limit;
            }
          }
        } catch (error) {
          console.error("Failed to load release MBID:", error.message);
          break;
        }
      }
    }

    loadReleaseMbid();
  }, [artistMbid, albumName, loadPhotos, loadTracks, fetchWithRetry]);



  useEffect(() => {
    async function loadWikiAlbum() {
      const normalizedArtistName = artistName.replace(
        /[^a-zA-Z0-9\s]/g,
        "-"
      );
      const encodedArtistName = encodeURIComponent(normalizedArtistName);

      const normalizedAlbumName = albumName.replace(
        /[^a-zA-Z0-9\s]/g,
        "-"
      );
      const encodedAlbumName =
        encodeURIComponent(normalizedAlbumName);
      try {
        const response = await fetch(
          `${import.meta.env.VITE_APP_API_HOST
          }/api/wikipedia_album/${encodedArtistName}+${encodedAlbumName}`
        );

        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }
        const data = await response.json();

        if (data && data.summary) {
          setWikiAlbum(data.summary);
        } else {
          console.log(
            `No Wikipedia information available for ${albumName}`
          );
          setWikiAlbum("No information available.");
        }
      } catch (error) {
        console.error(
          `There was a problem fetching Wikipedia data for ${albumName}:`,
          error
        );
        setWikiAlbum("No information available.");
      }
    }
    loadWikiAlbum()
  }, [artistName, albumName]);




  useEffect(() => {
    if (photos.length > 0 && tracks.length > 0) {
      setIsLoading(false);
      setRefetching(false)
    }
  }, [photos, tracks]);



  const toggleDescription = () => {
    setIsExpanded(!isExpanded);
  };


  const handleTabClick = (tabName) => {
    setActiveTab(tabName);
  };

  const handleAllReviewsClick = () => {
    setShowReviewModel(!showReviewModel)
  };


  const handleSizeClick = (url) => {
    openPhotoModal(url);
  };


  const handleNewReview = () => {
    setNewReviewSubmitted(prev => !prev);
  };

  function handleImageLoad() {
    setIsPhotoLoaded(true);
  }


  if (isLoading || refetching) {
    return (
      <div style={{ display: "flex", flexDirection: "column",
        alignItems: "center", justifyContent: "center", height: "100svh",
        marginTop: "-70px"
        }}
      >
        {refetching || isLoading && (
          <div style={{display: "flex", flexDirection: "column",
            alignItems: "center", overflow: "hidden", width: "100vw"
            }}
          >
            <img
            className="loadingCd"
            src="/cdArt1.png" />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <h1 style={{
                fontSize: "20px", marginTop: "40px",
                borderBottom: "1px solid white", borderRadius: "6px",
                padding: "0 4px"
              }}>
                Pressing Your Vinyl Now...
              </h1>
            </div>
          </div>
        )}
      </div>
    );
  }


  return (
    <div className="albumDetailDiv">
      <div className="albumDetailDivChild">
        <div className="albumDetailDivGridOne">
          <div className="albumDetailDivGridOneHeaderDiv">
            <h1 className="albumDetailDivGridOneHeader">{albumName}</h1>
          </div>
          <div className="albumDetailDivGridOneDescriptionDiv">
            <p className="albumDetailDivGridOneDescription">
              {isExpanded ? wikiAlbum : `${wikiAlbum.substring(0, 400)}`}
            </p>
          </div>
          {wikiAlbum.length > 400 && (
            <div className="showMoreButtonDiv">
              {isExpanded ? (
                <button
                  onClick={toggleDescription}
                  className="baseButtonArtist showMoreArrowDown"
                >
                  &#8963;
                </button>
              ) : (
                <button
                  onClick={toggleDescription}
                  className="baseButtonArtist showMoreArrowUp"
                >
                  &#8964;
                </button>
              )}
              <button onClick={toggleDescription} className="showMoreButton">
                {isExpanded ? "Show Less" : "Show More"}
              </button>
            </div>
          )}
        </div>
        {width < 700 && (
          <div className="tab-section">
            <div className="tabButtons">
              <ul className="tab-list">
                <li
                  className={`tab-item ${activeTab === "tracks" ? "active" : ""
                    }`}
                  onClick={() => handleTabClick("tracks")}
                >
                  Tracks
                </li>
                <li
                  className={`tab-item ${activeTab === "photos" ? "active" : ""
                    }`}
                  onClick={() => handleTabClick("photos")}
                >
                  Photos
                </li>
              </ul>
            </div>
            <div>
              <div className="tab-content">
                <div
                  className="tab-content-child"
                  style={{
                    display: activeTab === "tracks" ? "flex" : "none",
                  }}
                >
                  <div className="tracksMainDiv">
                    <div className="tracksHeaderDiv">
                      <h1 className="header">Tracks</h1>
                    </div>
                    <div className="tracksTableDiv">
                      <table className="tracksTable">
                        <tbody className="tracksTbody">
                          {tracks.map((track, index) => {
                            return (
                              <tr className="trBodyDiv" key={index}>
                                <td className="tdTrack">
                                  {index + 1}. {track}
                                </td>
                              </tr>
                            );
                          })}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-content-child-photos"
                  style={{
                    display: activeTab === "photos" ? "block" : "none",
                  }}
                >
                  <div className="albumDetailDivGridTwoChildOne">
                    <h1 className="header">Photos</h1>
                    <div
                      className={
                        photos.length <= 3
                          ? "photoAndSizesGridParentSmall"
                          : "photoAndSizesGridParentLarge"
                      }
                      >
                    {photos.map((photo, index) => {
                      if (photos.length <= 3) {
                        return (
                          <div
                            key={index}
                            className="photoAndSizesGridParentSmall"
                          >
                            <div className="photoAndSizesGrid">
                              <div>
                                {!isPhotoLoaded && (
                                  <img
                                    src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                                    style={{ opacity: ".3" }}
                                    alt="Loading"
                                    className="albumPhotoLarge"
                                  />
                                )}
                                <img
                                  key={photo.image}
                                  src={
                                    photo.image
                                      ? photo.image
                                      : defaultImage
                                  }
                                  alt="Loading"
                                  className="albumPhotoLarge"
                                  onLoad={handleImageLoad}
                                  style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                                />
                              </div>
                              <div>
                                <div className="sizesDiv">
                                  <button
                                    className="sizesLarge"
                                    onClick={() =>
                                      handleSizeClick(
                                        photo.thumbnails["250"]
                                          ? photo.thumbnails["250"]
                                          : photo.thumbnails.small
                                      )
                                    }
                                  >
                                    250px
                                  </button>
                                  <button
                                    className="sizesLarge"
                                    onClick={() =>
                                      handleSizeClick(
                                        photo.thumbnails["500"]
                                          ? photo.thumbnails["500"]
                                          : photo.thumbnails.small
                                            ? photo.thumbnails.small
                                            : photo.thumbnails.large
                                      )
                                    }
                                  >
                                    500px
                                  </button>
                                  <button
                                    className="sizesLarge"
                                    onClick={() =>
                                      handleSizeClick(
                                        photo.thumbnails["1200"]
                                          ? photo.thumbnails["1200"]
                                          : photo.thumbnails.large
                                            ? photo.thumbnails.large
                                            : photo.thumbnails.small
                                      )
                                    }
                                  >
                                    1200px
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        );
                      }
                      if (photos.length > 3) {
                        return (
                          <div
                            key={index}
                            className="photoAndSizesGridParent"
                          >
                            <div className="photoAndSizesGrid">
                              {!isPhotoLoaded && (
                                <img
                                  src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                                  style={{ opacity: ".3" }}
                                  alt="Loading"
                                  className="albumPhoto"
                                />
                              )}
                              <img
                                key={photo.image}
                                src={
                                  photo.image
                                    ? photo.image
                                    : defaultImage
                                }
                                alt="Loading"
                                className="albumPhoto"
                                onLoad={handleImageLoad}
                                style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                              />
                            </div>
                            <div>
                              <div className="sizesDiv">
                                <button
                                  className="sizes"
                                  onClick={() =>
                                    handleSizeClick(
                                      photo.thumbnails["250"]
                                        ? photo.thumbnails["250"]
                                        : photo.thumbnails.small
                                    )
                                  }
                                >
                                  250px
                                </button>
                                <button
                                  className="sizes"
                                  onClick={() =>
                                    handleSizeClick(
                                      photo.thumbnails["500"]
                                        ? photo.thumbnails["500"]
                                        : photo.thumbnails.large
                                          ? photo.thumbnails.large
                                          : photo.thumbnails.small
                                    )
                                  }
                                >
                                  500px
                                </button>
                                <button
                                  className="sizes"
                                  onClick={() =>
                                    handleSizeClick(
                                      photo.thumbnails["1200"]
                                        ? photo.thumbnails["1200"]
                                        : photo.thumbnails.large
                                          ? photo.thumbnails.large
                                          : photo.thumbnails.small
                                    )
                                  }
                                >
                                  1200px
                                </button>
                              </div>
                            </div>
                          </div>
                        );
                      }
                    })}
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        )}
        {width >= 700 && (
          <div className="albumDetailDivGridTwo">
            <div className="albumDetailDivGridTwoChild">
              <div className="albumDetailDivGridTwoChildOne">
                <h1 className="header">Photos</h1>
                <div
                  className={
                    photos.length <= 3
                      ? "photoAndSizesGridParentSmall"
                      : "photoAndSizesGridParentLarge"
                  }
                >
                  {photos.map((photo, index) => {
                    return photos.length <= 3 ? (
                      <div
                        className="photoAndSizesGrid"
                        key={index}
                      >
                        <div>
                          {!isPhotoLoaded && (
                            <img
                              src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                              style={{ opacity: ".3" }}
                              alt="Loading"
                              className="albumPhotoLarge"
                            />
                          )}
                          <img
                            key={photo.image}
                            src={
                              photo.image
                                ? photo.image
                                : defaultImage
                            }
                            alt="Loading"
                            className="albumPhotoLarge"
                            onLoad={handleImageLoad}
                            style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                          />
                        </div>
                        <div>
                          <div className="sizesDiv">
                            <button
                              className="sizesLarge"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["250"]
                                    ? photo.thumbnails["250"]
                                    : photo.thumbnails.small
                                )
                              }
                            >
                              250px
                            </button>
                            <button
                              className="sizesLarge"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["500"]
                                    ? photo.thumbnails["500"]
                                    : photo.thumbnails.small
                                      ? photo.thumbnails.small
                                      : photo.thumbnails.large
                                )
                              }
                            >
                              500px
                            </button>
                            <button
                              className="sizesLarge"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["1200"]
                                    ? photo.thumbnails["1200"]
                                    : photo.thumbnails.large
                                      ? photo.thumbnails.large
                                      : photo.thumbnails.small
                                )
                              }
                            >
                              1200px
                            </button>
                          </div>
                        </div>
                      </div>
                    ) : (
                      <div
                        key={index}
                        className="photoAndSizesGrid">
                        <div>
                          {!isPhotoLoaded && (
                            <img
                              src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                              style={{ opacity: ".3" }}
                              alt="Loading"
                              className="albumPhoto"
                            />
                          )}
                          <img
                            key={photo.image}
                            src={
                              photo.image
                                ? photo.image
                                : defaultImage
                            }
                            alt="Loading"
                            className="albumPhoto"
                            onLoad={handleImageLoad}
                            style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                          />
                        </div>
                        <div>
                          <div className="sizesDiv">
                            <button
                              className="sizes"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["250"]
                                    ? photo.thumbnails["250"]
                                    : photo.thumbnails.small
                                )
                              }
                            >
                              250px
                            </button>
                            <button
                              className="sizes"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["500"]
                                    ? photo.thumbnails["500"]
                                    : photo.thumbnails.small
                                      ? photo.thumbnails.small
                                      : photo.thumbnails.large
                                )
                              }
                            >
                              500px
                            </button>
                            <button
                              className="sizes"
                              onClick={() =>
                                handleSizeClick(
                                  photo.thumbnails["1200"]
                                    ? photo.thumbnails["1200"]
                                    : photo.thumbnails.large
                                      ? photo.thumbnails.large
                                      : photo.thumbnails.small
                                )
                              }
                            >
                              1200px
                            </button>
                          </div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              </div>
              <div className="albumDetailDivGridTwoChildTwo">
                <div className="tracksMainDiv">
                  <div className="tracksHeaderDiv">
                    <h1 className="header">Tracks</h1>
                  </div>
                  <div className="tracksTableDiv">
                    <table className="tracksTable">
                      <tbody className="tracksTbody">
                        {tracks.map((track, index) => {
                          return (
                            <tr
                              key={index}
                              className="trBodyDiv"
                            >
                              <td className="tdTrack">
                                {index + 1}. {track}
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="albumDetailDivGridThree">
          <div className="albumDetailDivGridThreeChild">
            <div className="averageReviewAndName">
              <h4 className="reviewHeader">Reviews</h4>
              <div className="starDiv">
                <Suspense fallback={<div>Loading...</div>}>
                  <AverageRating
                    releaseGroupMbid={releaseGroupMbid}
                    newReviewSubmitted={newReviewSubmitted}
                    starStyle={{
                      fontSize: "2em",
                      width: "28px",
                      height: "25px",
                      color: "gold",
                    }}
                  />
                </Suspense>
              </div>
            </div>
            <div className="newReviewDiv">
              <Suspense fallback={<div>Loading...</div>}>
                <ReviewForm
                  releaseGroupMbid={releaseGroupMbid}
                  onNewReview={handleNewReview}
                />
              </Suspense>
            </div>
            <div className="shoeAllReviews">
              <div className="averageNameAndReview">
                <button
                  onClick={handleAllReviewsClick}
                  className="averageReviewAllButton"
                >
                  All Reviews
                  {showReviewModel ? (
                    <span
                      style={{
                        color: "white",
                        fontSize: "13px",
                        marginLeft: "5px",
                      }}
                    >
                      &#9650;
                    </span>
                  ) : (
                    <span
                      style={{
                        color: "white",
                        fontSize: "13px",
                        marginLeft: "5px",
                      }}
                    >
                      &#9660;
                    </span>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="reviewsDiv">
          <Suspense fallback={<div>Loading...</div>}>
            {showReviewModel && (
              <Ratings
                releaseGroupMbid={releaseGroupMbid}
                newReviewSubmitted={newReviewSubmitted}
              />
            )}
          </Suspense>
        </div>
      </div>
    </div>
  );
}
export default AlbumDetail;
