import { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./ArtistDetailFinal.css";
import Loading from "../../extrafunctions/helpers/Loading";
import { useToast } from "../../context/toastcontext/useToast";
import { NavLink } from "react-router-dom";
import { useData } from "../../context/datacontext/useData";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import useWindowSize from "../../extrafunctions/helpers/WindowSize";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../../authorization/Authorization";

export default function ArtistDetail() {
    const [artist, setArtist] = useState({});
    const [albums, setAlbums] = useState([]);
    const [wikiArtist, setWikiArtist] = useState("");
    const [isLoading, setIsLoading] = useState(true);
    const [isExpanded, setIsExpanded] = useState(false);
    const [isPhotoLoaded, setIsPhotoLoaded] = useState(false);
    const { artistMbid, artistName, setReleaseGroupMbid, setAlbumName } = useData();
    const { token } = useAuthContext();
    const showToast = useToast();
    const { width } = useWindowSize();
    const navigate = useNavigate();
    const defaultImage = "https://st3.depositphotos.com/23594922/31822/v/450/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg"


    useEffect(() => {
        async function loadArtist() {
            const response = await fetch(`https://musicbrainz.org/ws/2/artist?query=artist:"${artistName}"&fmt=json`);
            if (response.ok) {
                const data = await response.json();
                setArtist(data.artists);
            }
        }
        loadArtist()
    }, [artistName]);


    useEffect(() => {
        async function loadAlbum() {
            const response = await fetch(`https://musicbrainz.org/ws/2/release-group?artist=${artistMbid}&fmt=json`);
            if (response.ok) {
                const data = await response.json();
                if (data) {
                    const albumsOnly = data["release-groups"].filter(
                        (item) =>
                            item["primary-type"] === "Album" ||
                            item["primary-type"] === "EP"
                    );
                    setAlbums(albumsOnly);
                } else {
                    console.error("Data Error")
                }
            } else {
                console.error("Response Error")
                loadAlbum()
            }
        }
        loadAlbum()
    }, [artistMbid]);


    useEffect(() => {
        async function loadWikiArtist() {
            const normalizedArtistName = artistName.replace(/[^a-zA-Z0-9\s]/g, '-');
            const encodedArtistName = encodeURIComponent(normalizedArtistName);
            const response = await fetch(`${import.meta.env.VITE_APP_API_HOST}/api/wikipedia_artist/${encodedArtistName}`);
            if (response.ok) {
                const data = await response.json();
                if (data && data.summary) {
                    setWikiArtist(data.summary);
                } else {
                    console.log(`No Wikipedia data available for ${artistName}`);
                    setWikiArtist("No information available.");
                }
            } else {
                console.log(`No Wikipedia response available for ${artistName}`);
                setWikiArtist("No information available.");
            }
        }
        loadWikiArtist()
    }, [artistName]);


    useEffect(() => {
        if (artist && albums.length > 0 && wikiArtist) {
            setIsLoading(false);
        }
    }, [artist, albums, wikiArtist]);



    useEffect(() => {
        let loadingTimeout;
        let refreshInterval;
        const savedRefreshAttempts = parseInt(localStorage.getItem('refreshAttempts') || '0', 3);
        if (isLoading) {
            loadingTimeout = setTimeout(() => {
                refreshInterval = setInterval(() => {
                    if (savedRefreshAttempts < 3) {
                        const newRefreshAttempts = savedRefreshAttempts + 1;
                        localStorage.setItem('refreshAttempts', newRefreshAttempts.toString());

                        window.location.reload();
                    } else {
                        clearInterval(refreshInterval);
                        localStorage.removeItem('refreshAttempts');
                    }
                }, 1500);
            }, 1500);
        } else {
            clearTimeout(loadingTimeout);
        }
        return () => {
            clearTimeout(loadingTimeout);
            clearInterval(refreshInterval);
        };
    }, [isLoading]);


    function albumClick(e) {
        const albumName = e.currentTarget.closest('#albumData').getAttribute('data-name');
        const releaseGroupMbid = e.currentTarget.closest('#albumData').getAttribute('data-release-group-mbid');

        if (albumName && releaseGroupMbid) {
            setReleaseGroupMbid(releaseGroupMbid)
            setAlbumName(albumName)
            navigate("/album")
        }
    }


    const handleAddClick = async (name, releaseGroupMbid) => {
        try {
            const response = await fetch(
                `${import.meta.env.VITE_APP_API_HOST}/api/favorite_album/create`,
                {
                    method: "POST",
                    credentials: "include",
                    body: JSON.stringify({
                        name: `${name}`,
                        artist_mbid: `${artistMbid}`,
                        artist_name: `${artistName}`,
                        release_group_mbid: `${releaseGroupMbid}`,
                        account_id: 0,
                    }),
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${token}`,
                    },
                }
            );
            if (response.ok) {
                showToast("Added To Favorites!", "success");
            } else {
                showToast("Could Not Add To Favorites!", "error");
            }
        } catch (error) {
            console.error("Error adding to favorites:", "error");
            showToast("You've Already Favorited This Artist!", "error");
        }
    };


    const toggleDescription = () => {
        setIsExpanded(!isExpanded);
    };

    function handleImageLoad() {
        setIsPhotoLoaded(true);
    }


    if (isLoading) {
        return (
            <div style={{ display: "flex", justifyContent: "center" }}>
                <Loading />
            </div>
        );
    }


    if (artist) {
        return (
            <div className="artistDetailDiv">
                <div className="artistDetailDivChild">
                    <div className="artistDetailDivGridOne">
                        <div className="artistDetailDivGridOneHeaderDiv">
                            <h1 className="artistDetailDivGridOneHeader">{artistName}</h1>
                        </div>
                        <div className="artistDetailDivGridOneDescriptionDiv">
                            <p className="artistDetailDivGridOneDescription">
                                {isExpanded ? wikiArtist : wikiArtist.substring(0, 400)}
                            </p>
                        </div>
                        {wikiArtist.length >= 400 && (
                            <div className="showMoreButtonDiv">
                                {isExpanded ? (
                                    <button
                                        onClick={toggleDescription}
                                        className="baseButtonArtist showMoreArrowDown"
                                    >
                                        &#8963;
                                    </button>
                                ) : (
                                    <button
                                        onClick={toggleDescription}
                                        className="baseButtonArtist showMoreArrowUp"
                                    >
                                        &#8964;
                                    </button>
                                )}
                                <button
                                    onClick={toggleDescription}
                                    className="showMoreButton"
                                >
                                    {isExpanded ? "Show Less" : "Show More"}
                                </button>
                            </div>
                        )}
                    </div>
                    {width <= 700 && (
                        <div className="tab-section">
                            <div className='artistGridMainHeadersAlbumsDiv'>
                                <h2 className='header'>
                                    Albums
                                </h2>
                            </div>
                            <div className='albumsDivParent'>
                                <div className='albumsDiv'>
                                    {albums.map(album => {
                                        const image = `https://coverartarchive.org/release-group/${album.id}/front`
                                        return (
                                            <div className="artistUlDiv"
                                                key={album.id}
                                                id='albumData'
                                                data-release-group-mbid={album.id}
                                                data-name={album.title}
                                            >
                                                <div className="searchedButtonDiv">
                                                    <FontAwesomeIcon
                                                        icon={faPlus}
                                                        onClick={() =>
                                                            handleAddClick(
                                                                album.title,
                                                                album.id
                                                            )
                                                        }
                                                        className="searchedButton"
                                                    />
                                                </div>
                                                <ul className="artistUL">
                                                    <li className="artistUlLiOne">
                                                        {!isPhotoLoaded && (
                                                            <img
                                                                src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                                                                alt="Loading"
                                                                className="albumTitlePhoto"
                                                                style={{ opacity: ".3" }}
                                                            />
                                                        )}
                                                        <img
                                                            key={image}
                                                            src={
                                                                image
                                                                    ? image
                                                                    : defaultImage
                                                            }
                                                            alt="Loading"
                                                            className="albumTitlePhoto"
                                                            onClick={albumClick}
                                                            onLoad={handleImageLoad}
                                                            style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                                                        />
                                                    </li>
                                                    <li className="artistUlLiTwo">
                                                        <NavLink className="albumNameLink" onClick={albumClick} to="/album">
                                                            {album.title}
                                                        </NavLink>
                                                    </li>
                                                </ul>
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                        </div>
                    )}
                    {width > 700 && (
                        <div className="artistDetailDivGridTwo">
                            <div className="artistDetailDivGridTwoChildOne">
                                <div className="artistGridMainHeadersAlbumsDiv">
                                    <h2 className="header">Albums</h2>
                                </div>
                                <div className="albumsDivParent">
                                    <div className="albumsDiv">
                                        {albums.map((album) => {
                                            const image = `https://coverartarchive.org/release-group/${album.id}/front`;
                                            return (
                                                <div
                                                    className="artistUlDiv"
                                                    key={album.id}
                                                    id="albumData"
                                                    data-release-group-mbid={
                                                        album.id
                                                    }
                                                    data-name={album.title}
                                                >
                                                    <div className="searchedButtonDiv">
                                                        <FontAwesomeIcon
                                                            icon={faPlus}
                                                            onClick={() =>
                                                                handleAddClick(
                                                                    album.title,
                                                                    album.id
                                                                )
                                                            }
                                                            className="searchedButton"
                                                        />
                                                    </div>
                                                    <ul className="artistUL">
                                                        <li className="artistUlLiOne">
                                                            {!isPhotoLoaded && (
                                                                <img
                                                                    src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                                                                    alt="Loading"
                                                                    className="albumTitlePhoto"
                                                                    style={{ opacity: ".3" }}
                                                                />
                                                            )}
                                                            <img
                                                                key={image}
                                                                src={
                                                                    image
                                                                        ? image
                                                                        : defaultImage
                                                                }
                                                                alt="Loading"
                                                                className="albumTitlePhoto"
                                                                onClick={albumClick}
                                                                onLoad={handleImageLoad}
                                                                style={{ display: isPhotoLoaded ? 'flex' : 'none' }}
                                                            />
                                                        </li>
                                                        <li className="artistUlLiTwo">
                                                            <NavLink
                                                                className="albumNameLink"
                                                                onClick={albumClick}
                                                                to="/album"
                                                            >
                                                                {album.title}
                                                            </NavLink>
                                                        </li>
                                                    </ul>
                                                </div>
                                            );
                                        })}
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}
