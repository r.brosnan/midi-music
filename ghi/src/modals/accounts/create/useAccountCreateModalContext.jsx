import { useContext } from "react";
import { AccountCreateModalContext } from "./AccountCreateModalContext";

export function useAccountCreateModalContext() {
  return useContext(AccountCreateModalContext);
}
