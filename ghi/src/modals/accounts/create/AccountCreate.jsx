import { useState } from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import useToken from "../../../authorization/Authorization";
import { useAccountCreateModalContext } from "./useAccountCreateModalContext";
import { useAccountLoginModalContext } from "../login/useAccountLoginModalContext";
import "./AccountCreateModal.css";

function AccountCreate() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState();
  const { login } = useToken();
  const { closeAccountCreateModal } = useAccountCreateModalContext();
  const { openAccountLoginModal } = useAccountLoginModalContext();


  const handleRegistration = async (e) => {
    e.preventDefault();
    const accountData = {
      username: username,
      password: password,
    };
    try {
      const response = await fetch(
        `${import.meta.env.VITE_APP_API_HOST}/api/account`,
        {
          method: "POST",
          credentials: "include",
          body: JSON.stringify(accountData),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (!response.ok) {
        setErrorMessage(
          "Couldn't create account, please try a new username"
        );
        return;
      }
      await login(accountData.username, accountData.password);
      e.target.reset();
      closeAccountCreateModal();
    } catch (e) {
      setErrorMessage(
        "Couldn't create account, please try a new username or email address"
      );
    }
  };


  return (
    <div className="card text-bg-light mb-3 darkBackground">
      <div className="card-header createAccountAndCloseDiv darkBackground">
        <h5 className="createAccountHeader whiteText">Create Account</h5>
        <FontAwesomeIcon
          icon={faXmark}
          className="closeButton" onClick={closeAccountCreateModal}
        />
      </div>
      <div className="card-body darkBackground">
        <form onSubmit={(e) => handleRegistration(e)}>
          {errorMessage ? <p>{errorMessage}</p> : ""}
          <div className="mb-3 whiteText">
            <label className="form-label">Username:</label>
            <input
              name="username"
              type="text"
              className="form-control"
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="mb-3 whiteText">
            <label className="form-label">Password:</label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="signupAndLoginDiv">
            <div>
              <input className="btn btn-primary" type="submit" value="Signup" />
            </div>
            <div className="loginDiv">
              <h4 className="haveAnAccountHeader whiteText">Have An Account?</h4>
              <input className="btn btn-primary" type="button" value="Login" onClick={() => {closeAccountCreateModal(), openAccountLoginModal()}}/>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
export default AccountCreate;
