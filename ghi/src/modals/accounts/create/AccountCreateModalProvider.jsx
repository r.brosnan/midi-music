import { useState } from "react";
import AccountCreate from "./AccountCreate";
import "./AccountCreateModal.css";
import PropTypes from 'prop-types';
import { AccountCreateModalContext } from "./AccountCreateModalContext";


function AccountCreateModal({ isAccountCreateModalOpen, closeAccountCreateModal }) {
  return (
    <div className={isAccountCreateModalOpen ? "signInModel show" : "signInModel"}>
      <div>
        <AccountCreate closeAccountCreateModal={closeAccountCreateModal} />
      </div>
    </div>
  );
}

AccountCreateModal.propTypes = {
  isAccountCreateModalOpen: PropTypes.bool.isRequired,
  closeAccountCreateModal: PropTypes.func.isRequired,
};

export function AccountCreateModalProvider({ children }) {
  const [isAccountCreateModalOpen, setIsAccountCreateModalOpen] = useState(false);

  const openAccountCreateModal = () => setIsAccountCreateModalOpen(true);
  const closeAccountCreateModal = () => setIsAccountCreateModalOpen(false);

  return (
    <AccountCreateModalContext.Provider
      value={{ isAccountCreateModalOpen, openAccountCreateModal, closeAccountCreateModal }}
    >
      {children}
      <AccountCreateModal isAccountCreateModalOpen={isAccountCreateModalOpen} closeAccountCreateModal={closeAccountCreateModal} />
    </AccountCreateModalContext.Provider>
  );
}


AccountCreateModalProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
