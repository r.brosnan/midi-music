import { useContext } from "react";
import { AccountLoginModalContext } from "./AccountLoginModalContext"

export function useAccountLoginModalContext() {
    return useContext(AccountLoginModalContext);
}
