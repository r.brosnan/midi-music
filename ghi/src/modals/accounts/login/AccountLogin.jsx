import { useState } from "react";
import "./AccountLoginModal.css";
import { useAccountLoginModalContext } from "./useAccountLoginModalContext";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { useToast } from "../../../context/toastcontext/useToast";
import useToken from "../../../authorization/Authorization";


function AccountLogin() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [errorMessage, setErrorMessage] = useState("");
  const { login } = useToken();
  const { closeAccountLoginModal } = useAccountLoginModalContext();
  const showToast = useToast();

  const handleSubmit = async (e) => {
    setErrorMessage("")
    e.preventDefault();
    const isLoginSuccessful = await login(username, password);
    e.target.reset();
    if (isLoginSuccessful) {
      closeAccountLoginModal();
      showToast(`Welcome, ${username}`, "success");
    } else {
      setErrorMessage("Invalid Login")
      showToast("Invalid Username Or Password!", "error");
    }
  };


  return (
    <div className="card text-bg-light mb-3 darkBackground">
      <div className="card-header logInAndCloseDiv darkBackground">
        <h5 className="logInHeader whiteText">Login</h5>
        <FontAwesomeIcon
          icon={faXmark}
          className="closeButton " onClick={closeAccountLoginModal}
        />
      </div>
      <div className="card-body darkBackground">
        <form onSubmit={(e) => handleSubmit(e)}>
          <div className="mb-3">
            <label className="form-label whiteText">Username:</label>
            <input
              name="username"
              type="text"
              className="form-control"
              onChange={(e) => setUsername(e.target.value)}
              onClick={() => setErrorMessage("")}
            />
          </div>
          <div className="mb-3 whiteText">
            <label className="form-label">Password:</label>
            <input
              name="password"
              type="password"
              className="form-control"
              onChange={(e) => setPassword(e.target.value)}
              onClick={() => setErrorMessage("")}
            />
          </div>
          <div style={{ display: "flex" }}>
            <div className="loginAndSignupDiv ">
              <div className="loginDiv2">
                <input className="btn btn-primary" type="submit" value="Login" onClick={closeAccountLoginModal} />
              </div>
            </div>
            <div style={{ display: "flex", alignItems: "center" }}>
              {errorMessage && (
                <h4 style={{ color: "red", fontSize: "15px", marginLeft: "15px" }}>
                  {errorMessage}
                </h4>
              )}
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
export default AccountLogin;
