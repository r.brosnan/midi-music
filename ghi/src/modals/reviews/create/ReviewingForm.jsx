import { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faX } from "@fortawesome/free-solid-svg-icons";
import { faStar as farFaStar } from "@fortawesome/free-regular-svg-icons";
import { faStar as fasFaStar } from "@fortawesome/free-solid-svg-icons";
import { faStarHalfAlt } from "@fortawesome/free-solid-svg-icons";
import "./ReviewingForm.css";
import "react-toastify/dist/ReactToastify.css";
import { useToast } from "../../../context/toastcontext/useToast";
import { useAccountLoginModalContext } from "../../accounts/login/useAccountLoginModalContext";
import { useAuthContext } from "../../../authorization/Authorization";
import PropTypes from "prop-types";


function ReviewingForm({ releaseGroupMbid, onNewReview }) {
  const [hoveredStar, setHoveredStar] = useState(null);
  const [selectedStar, setSelectedStar] = useState(null);
  const [showModel, setShowModel] = useState(false);
  const showToast = useToast();
  const { openAccountLoginModal } = useAccountLoginModalContext();
  const { token } = useAuthContext();
  const [formData, setFormData] = useState({
    rating: 0,
    comment: "",
    release_group_mbid: releaseGroupMbid,
    account_username: "",
  });

  const handleSubmit = async (event) => {
    event.preventDefault();
    if (token) {
      const ratingUrl = `${import.meta.env.VITE_APP_API_HOST
        }/api/review/create`;
      const fetchConfig = {
        method: "POST",
        body: JSON.stringify(formData),
        credentials: 'include',
        headers: {
          "Content-Type": "application/json",
        },
      };
      const response = await fetch(ratingUrl, fetchConfig);
      if (response.ok) {
        showToast("Thanks For Your Review!", "success");
        onNewReview();
        setFormData({
          rating: 0,
          comment: "",
          release_group_mbid: "",
          account_username: "",
        });
        setShowModel(false);
      } else {
        showToast("You've Already Reviewed This Album", "error");
      }
    }
  };

  useEffect(() => {
    setFormData((prevFormData) => ({
      ...prevFormData,
      rating: selectedStar,
    }));
  }, [selectedStar]);

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      rating: selectedStar,
      [inputName]: value,
    });
  };

  const handleCreateReviewClick = () => {
    if (!token) {
      openAccountLoginModal()
      showToast("Please log in first!", "error");
    }
    else {
      setShowModel(true);
    }
  };

  const handleCloseClick = () => {
    setShowModel(false);
  };

  const handleStarClick = (e, starNumber) => {
    e.preventDefault();
    const starElement = e.currentTarget.getBoundingClientRect();
    const starHalfWidth = starElement.width / 2;
    const clickPosition = e.clientX - starElement.left;

    if (clickPosition < starHalfWidth) {
      setSelectedStar(starNumber - 0.5);
    } else {
      setSelectedStar(starNumber);
    }
  };

  const handleStarMouseMove = (e, starNumber) => {
    const starElement = e.currentTarget.getBoundingClientRect();
    const starHalfWidth = starElement.width / 2;
    const hoverPosition = e.clientX - starElement.left;

    if (hoverPosition < starHalfWidth) {
      setHoveredStar(starNumber - 0.5);
    } else {
      setHoveredStar(starNumber);
    }
  };

  ReviewingForm.propTypes = {
    releaseGroupMbid: PropTypes.string.isRequired,
    onNewReview: PropTypes.func.isRequired,
  };

  return (
    <div className="products_star">
      <div className="newReviewDivForm">
        <button className="newReviewForm" onClick={handleCreateReviewClick}>
          Write a Review
        </button>
      </div>
      {showModel && <div className="overlay"></div>}
      <div className={showModel ? "ratingModel show" : "ratingModel"}>
        <div className="topSection">
          <div className="modelWindowXAndRate">
            <div className="rateStarsDiv">
              <h2 className="rateStars">Review</h2>
            </div>
            <div className="exitButtonDiv">
              <FontAwesomeIcon
                onClick={handleCloseClick}
                className="exitButton"
                size="1x"
                icon={faX}
              />
            </div>
          </div>
        </div>
        <div>
          {[1, 2, 3, 4, 5].map((starNumber) => {
            let iconToUse = farFaStar;
            const currentRating = hoveredStar;
            if (currentRating >= starNumber) {
              iconToUse = fasFaStar;
            } else if (currentRating >= starNumber - 0.5) {
              iconToUse = faStarHalfAlt;
            }
            return (
              <FontAwesomeIcon
                key={starNumber}
                className="starIcon1 fa-fw"
                icon={iconToUse}
                onMouseMove={(e) => handleStarMouseMove(e, starNumber)}
                onMouseLeave={() => setHoveredStar(selectedStar)}
                onClick={(e) => handleStarClick(e, starNumber)}
                color="transparent"
                style={{
                  fontSize: "25px",
                  color: iconToUse !== farFaStar ? "gold" : "white",
                }}
              />
            );
          })}
        </div>
        <form onSubmit={handleSubmit} id="sign-in-form">
          <div className="ratingContent">
            <div className="ratingDescDiv">
              <textarea
                className="ratingDesc form-control"
                placeholder="Describe your rating..."
                onChange={handleFormChange}
                value={formData.comment}
                required
                type="text"
                name="comment"
                id="comment"
              ></textarea>
            </div>
            <div className="submitRatingDiv">
              <button className="submitRating" onClick={handleSubmit}>
                Submit
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}

export default ReviewingForm;
