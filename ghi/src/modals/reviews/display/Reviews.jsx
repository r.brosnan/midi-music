import { useState, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStar as farFaStar } from '@fortawesome/free-regular-svg-icons';
import { faStar as fasFaStar } from '@fortawesome/free-solid-svg-icons';
import { faStarHalfAlt } from '@fortawesome/free-solid-svg-icons';
import "../create/ReviewingForm.css";
import moment from 'moment';
import PropTypes from 'prop-types';


function formatDate(dateString) {
    return moment(dateString).format("llll");
}


export default function Reviews({ starStyle = {}, releaseGroupMbid, newReviewSubmitted }) {
    const [ratings, setRatings] = useState([]);



    useEffect(() => {
        async function loadRating() {
            try {
                const response = await fetch(`${import.meta.env.VITE_APP_API_HOST}/api/${releaseGroupMbid}/review`
                );
                if (response.ok) {
                    const data = await response.json();
                    if (data && data.length > 0) {
                        setRatings(data);
                    } else {
                        setRatings(null);
                    }
                } else {
                    throw new Error('Response was not ok');
                }
            } catch (error) {
                console.error('Cannot get the ratings:', error);
            }
        }
        loadRating();
    }, [releaseGroupMbid, newReviewSubmitted]);


Reviews.propTypes = {
  starStyle: PropTypes.object,
  releaseGroupMbid: PropTypes.string.isRequired,
  newReviewSubmitted: PropTypes.bool.isRequired,
};


    return (
        <div>
            {ratings?.map(oneRating => {
                const formattedDate = formatDate(oneRating.created_at)
                return (
                    <div key={oneRating.id} className='reviewsDivChild'>
                        <div className='reviewsNameDiv'>
                            <h6 className='reviewsName'>
                            {oneRating.account_username}
                            </h6>
                        </div>
                        <div style={{ display: "flex", position: "relative", justifyContent: "space-between", }}>
                            <div className="review_stars">
                                {[1, 2, 3, 4, 5].map((starNumber) => {
                                    let iconToUse = farFaStar;
                                    const currentRating = oneRating.rating;
                                    if (currentRating >= starNumber) {
                                        iconToUse = fasFaStar;
                                    } else if (currentRating >= starNumber - 0.5) {
                                        iconToUse = faStarHalfAlt;
                                    }
                                    return (
                                        <FontAwesomeIcon
                                            key={starNumber}
                                            className="starIcon fa-fw"
                                            icon={iconToUse}
                                            color="transparent"
                                            style={{
                                                color: iconToUse !== farFaStar ? 'gold' : 'white',
                                                ...starStyle
                                            }}
                                        />
                                    );
                                })}
                            </div>
                            <div style={{display: "flex", alignItems: "center", position: "relative", top: "-5px"}}>
                                <h4 style={{fontSize: "15px"}}>
                                {formattedDate}
                                </h4>
                            </div>
                        </div>
                        <div className='reviewsDescriptionDiv'>
                            <p className='reviewsDescription'>{oneRating.comment}</p>
                        </div>
                    </div>
                )
            })
            }
        </div>
    );
}

Reviews.propTypes = {
  children: PropTypes.node.isRequired,
};
