import "./PhotoModalProvider.css"
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import PropTypes from 'prop-types';
import { PhotoModalContext } from "./PhotoContext"


export function PhotoModalProvider({ children }) {
    const [isPhotoModalOpen, setIsPhotoModalOpen] = useState(false);
    const [isPhotoLoaded, setIsPhotoLoaded] = useState(false);
    const [currentPhoto, setCurrentPhoto] = useState(null);

    const openPhotoModal = (url) => {
        setCurrentPhoto(url);
        setIsPhotoModalOpen(true);
        setIsPhotoLoaded(false);
    };

    const closePhotoModal = () => {
        setCurrentPhoto(null);
        setIsPhotoModalOpen(false);
        setIsPhotoLoaded(false);
    };

    function handleXClick() {
        closePhotoModal();
    }

    function handleImageLoad() {
        setIsPhotoLoaded(true);
    }


    function PhotoModal() {
        return (
            <div className={isPhotoModalOpen ? "photoModel show" : "photoModel"}>
                <div className="closeButtonPhotoDiv">
                    <FontAwesomeIcon
                    icon={faXmark}
                    className="closeButtonPhoto" onClick={handleXClick}
                    />
                </div>
                <div className="modalPhotoDiv">
                    {!isPhotoLoaded && (
                        <img
                            src="https://media.giphy.com/media/McUBKCpESJD0F7eqzT/giphy.gif"
                            alt="Loading"
                            className="modalPhotoLoading"
                        />
                    )}
                    <img
                        key={currentPhoto}
                        src={currentPhoto}
                        className="modalPhoto"
                        alt="Album Photo"
                        onLoad={handleImageLoad}
                        style={{ display: isPhotoLoaded ? 'block' : 'none' }}
                    />
                </div>
            </div>
        );
    }

    return (
        <PhotoModalContext.Provider value={{ isPhotoModalOpen, openPhotoModal, closePhotoModal, currentPhoto }}>
            {children}
            <PhotoModal isModalOpen={isPhotoModalOpen} closeModal={closePhotoModal} photoUrl={currentPhoto} />
        </PhotoModalContext.Provider>
    );
}

PhotoModalProvider.propTypes = {
    children: PropTypes.node.isRequired,
};
