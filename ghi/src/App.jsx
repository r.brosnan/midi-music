import { BrowserRouter, Routes, Route } from "react-router-dom";
import "react-toastify/dist/ReactToastify.css";
import { ToastProvider } from "./context/toastcontext/ToastProvider";
import { ToastContainer } from "react-toastify";
import Nav from "./main/nav/Nav";
import MainPage from "./main/page/MainPage";
import Searched from "./pages/searched/Searched";
import FavoriteAlbums from "./pages/favorites/FavoriteAlbums";
import FavoriteArtists from "./pages/favorites/FavoriteArtists";
import ArtistDetail from "./pages/artists/ArtistDetailFinal";
import AlbumDetail from "./pages/albums/AlbumDetail";
import { AccountLoginModalProvider } from "./modals/accounts/login/AccountLoginModalProvider";
import { AccountCreateModalProvider } from "./modals/accounts/create/AccountCreateModalProvider";
import { PhotoModalProvider } from "./modals/photos/PhotoModalProvider";
import { AuthProvider } from "./authorization/Authorization";
import "./App.css";


const TokenURL = import.meta.env.VITE_APP_API_HOST;
if (!TokenURL) {
  throw Error("VITE_APP_API_HOST was undefined ");
}

function App() {
  const domain = /https:\/\/[^/]+/;
  const basename = import.meta.env.VITE_PUBLIC_URL.replace(domain, '');



  return (
    <BrowserRouter basename={basename}>
      <AuthProvider baseUrl={TokenURL}>
          <ToastProvider>
            <PhotoModalProvider>
              <AccountLoginModalProvider>
                <AccountCreateModalProvider>
                    <Nav />
                    <ToastContainer />
                    <Routes>
                      <Route path="/">
                        <Route index element={<MainPage />} />
                      </Route>
                      <Route path="artist">
                        <Route index element={<ArtistDetail />} />
                      </Route>
                      <Route path="album">
                        <Route index element={<AlbumDetail/>} />
                      </Route>
                      <Route path="search">
                        <Route index element={<Searched />} />
                      </Route>
                      <Route path="favorite">
                        <Route path="albums" element={<FavoriteAlbums />} />
                        <Route path="artists" element={<FavoriteArtists />} />
                      </Route>
                    </Routes>
                </AccountCreateModalProvider>
              </AccountLoginModalProvider>
            </PhotoModalProvider>
          </ToastProvider>
      </AuthProvider >
    </BrowserRouter>
  );
}

export default App;
