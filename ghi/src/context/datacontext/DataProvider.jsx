import { useState, useEffect } from 'react';
import { DataContext } from "./DataContext";
import PropTypes from 'prop-types';


export const DataProvider = ({ children }) => {
    const [searchInput, setSearchInput] = useState('');
    const [artistName, setArtistName] = useState('');
    const [artistMbid, setArtistMbid] = useState('');
    const [albumName, setAlbumName] = useState('');
    const [releaseGroupMbid, setReleaseGroupMbid] = useState('');
    const [releaseMbid, setReleaseMbid] = useState('');


    useEffect(() => {
        const savedArtistMbid = localStorage.getItem('artistMbid');
        const savedArtistName = localStorage.getItem('artistName');
        const savedReleaseGroupMbid = localStorage.getItem('releaseGroupMbid');
        const savedReleaseMbid = localStorage.getItem('releaseMbid');
        const savedAlbumName = localStorage.getItem('albumName');

        if (savedArtistMbid) {
            setArtistMbid(savedArtistMbid);
        }
        if (savedArtistName) {
            setArtistName(savedArtistName);
        }
        if (savedReleaseGroupMbid) {
            setReleaseGroupMbid(savedReleaseGroupMbid);
        }
        if (savedReleaseMbid) {
            setReleaseMbid(savedReleaseMbid);
        }
        if (savedAlbumName) {
            setAlbumName(savedAlbumName);
        }
    }, []);

    useEffect(() => {
        localStorage.setItem('artistMbid', artistMbid);
    }, [artistMbid]);

    useEffect(() => {
        localStorage.setItem('artistName', artistName);
    }, [artistName]);

    useEffect(() => {
        localStorage.setItem('releaseGroupMbid', releaseGroupMbid);
    }, [releaseGroupMbid]);

    useEffect(() => {
        localStorage.setItem('releaseMbid', releaseMbid);
    }, [releaseMbid]);

    useEffect(() => {
        localStorage.setItem('albumName', albumName);
    }, [albumName]);


    const values = {
        searchInput,
        setSearchInput,
        artistName,
        setArtistName,
        artistMbid,
        setArtistMbid,
        releaseGroupMbid,
        setReleaseGroupMbid,
        releaseMbid,
        setReleaseMbid,
        albumName,
        setAlbumName,
    }


    return (
        <DataContext.Provider value={values}>
            {children}
        </DataContext.Provider>
    );
};

DataProvider.propTypes = {
    children: PropTypes.node.isRequired,
};
