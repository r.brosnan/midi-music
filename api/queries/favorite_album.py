from pydantic import BaseModel
from typing import List, Union
from queries.pool import pool
from fastapi import (
    HTTPException,
    status,
)


class Error(BaseModel):
    message: str


class FavoriteAlbumIn(BaseModel):
    name: str
    artist_name: str
    release_group_mbid: str
    artist_mbid: str
    account_id: int


class FavoriteAlbumOut(BaseModel):
    id: int
    name: str
    artist_name: str
    release_group_mbid: str
    artist_mbid: str
    account_id: int


class WikipediaSummaryResponse(BaseModel):
    summary: str


class FavoriteAlbumRepository:
    def favorite_album_exists(self, release_group_mbid, account_id) -> bool:
        with pool.connection() as conn:
            with conn.cursor() as db:
                db.execute(
                    """
                    SELECT EXISTS (
                        SELECT 1 FROM favorite_album
                        WHERE release_group_mbid = %s AND account_id = %s
                    );
                    """,
                    [release_group_mbid, account_id],
                )
                return db.fetchone()[0]

    def create(self, favorite_album: FavoriteAlbumIn) -> FavoriteAlbumOut:
        if self.favorite_album_exists(
            favorite_album.release_group_mbid, favorite_album.account_id
        ):
            raise HTTPException(
                status_code=status.HTTP_400_BAD_REQUEST,
                detail="User has already favorited this Album",
            )
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        INSERT INTO favorite_album
                            (name, artist_name, release_group_mbid,
                            artist_mbid, account_id)
                        VALUES
                            (%s, %s, %s, %s, %s)
                        RETURNING id;
                        """,
                        [
                            favorite_album.name,
                            favorite_album.artist_name,
                            favorite_album.release_group_mbid,
                            favorite_album.artist_mbid,
                            favorite_album.account_id,
                        ],
                    )
                    id = result.fetchone()[0]
                    return self.favorite_album_in_to_out(id, favorite_album)
        except Exception as e:
            print(e)
            return {"message": "Could not create favorite_album"}

    def get_all(self, account_id: int) -> Union[List[FavoriteAlbumOut], Error]:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , name
                            , artist_name
                            , release_group_mbid
                            , artist_mbid
                            , account_id
                        FROM favorite_album
                        WHERE account_id = %s
                        """,
                        [account_id],
                    )
                    result = []
                    for record in db:
                        favorite_album = FavoriteAlbumOut(
                            id=record[0],
                            name=record[1],
                            artist_name=record[2],
                            release_group_mbid=record[3],
                            artist_mbid=record[4],
                            account_id=record[5],
                        )
                        result.append(favorite_album)
                    return result
        except Exception as e:
            print(e)
            return {"message": "Could not get all favorite_album"}

    def delete(self, favorite_album_id: int) -> bool:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    db.execute(
                        """
                        DELETE FROM favorite_album
                        WHERE id = %s
                        """,
                        [favorite_album_id],
                    )
                    return True

        except Exception as e:
            print(e)
            return False

    def favorite_album_in_to_out(
        self, id: int, favorite_album: FavoriteAlbumIn
    ):
        old_data = favorite_album.dict()
        return FavoriteAlbumOut(id=id, **old_data)
