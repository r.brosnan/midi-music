from pydantic import BaseModel
from queries.pool import pool


class Error(BaseModel):
    message: str


class DuplicateAccountError(ValueError):
    pass


class AccountIn(BaseModel):
    username: str
    password: str


class AccountOut(BaseModel):
    id: int
    username: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str


class AccountRepository:
    def create(
        self, account: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        with pool.connection() as conn:
            with conn.cursor() as db:
                result = db.execute(
                    """
                    INSERT INTO account
                        (username, hashed_password)
                    VALUES
                        (%s, %s)
                    RETURNING id;
                    """,
                    [account.username, hashed_password],
                )
                id = result.fetchone()[0]
                return self.account_in_to_out(id, account)

    def get_one(self, username: str) -> AccountOut:
        try:
            with pool.connection() as conn:
                with conn.cursor() as db:
                    result = db.execute(
                        """
                        SELECT id
                            , username
                            , hashed_password
                        FROM account
                        WHERE username = %s
                        """,
                        [username],
                    )
                    record = result.fetchone()
                    return AccountOutWithPassword(
                        id=record[0],
                        username=record[1],
                        hashed_password=record[2],
                    )

        except Exception as e:
            print(e)
            return {"message": "Could not retrieve account"}

    def account_in_to_out(self, id: int, account: AccountIn):
        old_data = account.dict()
        return AccountOut(id=id, **old_data)
