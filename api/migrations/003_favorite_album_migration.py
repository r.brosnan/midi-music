steps = [
    [
        # "Up" SQL statement
        """
            CREATE TABLE IF NOT EXISTS favorite_album (
                id SERIAL PRIMARY KEY,
                name VARCHAR(1000) NOT NULL,
                artist_name VARCHAR(1000) NOT NULL,
                release_group_mbid VARCHAR(100) NOT NULL,
                artist_mbid VARCHAR(100) NOT NULL,
                account_id INTEGER REFERENCES account(id) ON DELETE CASCADE
                    NOT NULL
            );
        """,
        # "Down" SQL statement
        """
            DROP TABLE favorite_album;
        """
    ]
]
