steps = [
    [
        # "Up" SQL statement
        """
            CREATE TABLE IF NOT EXISTS account (
                id SERIAL PRIMARY KEY,
                username VARCHAR(150) UNIQUE NOT NULL,
                hashed_password VARCHAR(150) NOT NULL
            );
        """,
        # "Down" SQL statement
        """
            DROP TABLE account;
        """
    ]
]
