from fastapi import APIRouter, Depends, Response
from typing import Union, List
from authenticator import authenticator
import wikipedia
from queries.favorite_album import (
    Error,
    FavoriteAlbumIn,
    FavoriteAlbumOut,
    FavoriteAlbumRepository,
    WikipediaSummaryResponse,
)


router = APIRouter()


@router.post(
    "/api/favorite_album/create", response_model=Union[FavoriteAlbumOut, Error]
)
def create_favorite_album(
    favorite_album: FavoriteAlbumIn,
    response: Response,
    repo: FavoriteAlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        favorite_album.account_id = account_data.get("id")
        result = repo.create(favorite_album)
        response.status_code = 200
        return result
    except Exception as e:
        print(f"Error creating favorite album: {e}")
        response.status_code = 500
        return {"error": "Internal Server Error"}


@router.get(
    "/api/user/favorite_album",
    response_model=Union[List[FavoriteAlbumOut], Error],
)
def get_all_favorite_album(
    repo: FavoriteAlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        return repo.get_all(account_data.get("id"))
    except Exception as e:
        print(e)
        return {"message": "Could not get all favorite_album"}


@router.delete(
    "/api/favorite_album/{favorite_album_id}/delete", response_model=bool
)
def delete_favorite_album(
    favorite_album_id: int,
    repo: FavoriteAlbumRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(favorite_album_id)


@router.get(
    "/api/wikipedia_album/{search_album_artist}",
    response_model=WikipediaSummaryResponse,
)
def get_wikipedia_album(
    search_album_artist: str,
    # This needs to be concatenate in the front
    # end before being sent to the backend. Format = '"ArtistName"+"AlbumName"'
):
    try:
        summary = wikipedia.summary(search_album_artist)
        return WikipediaSummaryResponse(summary=summary)
    except wikipedia.exceptions.DisambiguationError as e:
        error_message = f"DisambiguationError: {e}"
        return WikipediaSummaryResponse(summary=error_message)
    except wikipedia.exceptions.HTTPTimeoutError as e:
        error_message = f"HTTPTimeoutError: {e}"
        return WikipediaSummaryResponse(summary=error_message)
    except wikipedia.exceptions.PageError as e:
        error_message = f"PageError: {e}"
        return WikipediaSummaryResponse(summary=error_message)
    except Exception as e:
        error_message = f"An error occurred: {e}"
        return WikipediaSummaryResponse(summary=error_message)
