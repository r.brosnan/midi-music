from fastapi import APIRouter, Depends, HTTPException, Response
from typing import Union, List
from authenticator import authenticator
import wikipedia
from queries.favorite_artist import (
    Error,
    FavoriteArtistIn,
    FavoriteArtistOut,
    FavoriteArtistRepository,
    WikipediaSummaryResponse,
)


router = APIRouter()


@router.post(
    "/api/favorite_artist/create",
    response_model=Union[FavoriteArtistOut, Error],
)
def create_favorite_artist(
    favorite_artist: FavoriteArtistIn,
    response: Response,
    repo: FavoriteArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        response.status_code = 200
        favorite_artist.account_id = account_data.get("id")
        return repo.create(favorite_artist)

    except Exception as e:
        print(e)
        response.status_code = 500
        return {"message": "Could not create favorite_artist"}


@router.get(
    "/api/user/favorite_artist",
    response_model=Union[List[FavoriteArtistOut], Error],
)
def get_all_favorite_artists(
    repo: FavoriteArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
):
    try:
        return repo.get_all(account_data.get("id"))
    except Exception as e:
        print(e)
        return {"message": "Could not get all favorite_artist"}


@router.delete(
    "/api/favorite_artist/{favorite_artist_id}/delete", response_model=bool
)
def delete_favorite_artist(
    favorite_artist_id: int,
    repo: FavoriteArtistRepository = Depends(),
    account_data: dict = Depends(authenticator.get_current_account_data),
) -> bool:
    return repo.delete(favorite_artist_id)


@router.get(
    "/api/wikipedia_artist/{search_artist}",
    response_model=WikipediaSummaryResponse,
)
def get_wikipedia_artist(
    search_artist: str,
):
    try:
        summary = wikipedia.summary(search_artist, auto_suggest=False)
        return WikipediaSummaryResponse(summary=summary)
    except wikipedia.exceptions.DisambiguationError as e:
        return HTTPException(
            status_code=400, detail=f"DisambiguationError: {e}"
        )
    except wikipedia.exceptions.HTTPTimeoutError as e:
        raise HTTPException(status_code=500, detail=f"HTTPTimeoutError: {e}")
    except wikipedia.exceptions.PageError as e:
        raise HTTPException(status_code=404, detail=f"PageError: {e}")
    except Exception as e:
        raise HTTPException(status_code=500, detail=f"An error occurred: {e}")
