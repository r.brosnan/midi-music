import os
from fastapi import Depends
from jwtdown_fastapi.authentication import Authenticator
from queries.account import (
    AccountRepository,
    AccountOut,
    AccountOutWithPassword,
)


class MidiAuthenticator(Authenticator):
    async def get_account_data(
        self,
        username: str,
        account: AccountRepository,
    ):
        return account.get_one(username)

    def get_account_getter(
        self,
        account: AccountRepository = Depends(),
    ):
        return account

    def get_hashed_password(self, account: AccountOutWithPassword):
        return account.hashed_password

    def get_account_data_for_cookie(self, account: AccountOut):
        return account.username, AccountOut(**account.dict())


authenticator = MidiAuthenticator(os.environ["SIGNING_KEY"])
