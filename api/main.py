from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from authenticator import authenticator
import os
from routers import account, favorite_artist, favorite_album, review


app = FastAPI()


app.include_router(authenticator.router)
app.include_router(account.router)
app.include_router(favorite_artist.router)
app.include_router(favorite_album.router)
app.include_router(review.router)


app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST"), "http://localhost:3000"
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    return {"message": "You hit the root path! MIDI"}
