from fastapi.testclient import TestClient
from unittest.mock import patch
import wikipedia
from main import app

client = TestClient(app)


def mock_wikipedia_summary(title, *args, **kwargs):
    if title == "Valid Album":
        return "This is a summary for a valid album."
    elif title == "Disambiguation":
        raise wikipedia.exceptions.DisambiguationError(title, [])
    elif title == "HTTPTimeout":
        raise wikipedia.exceptions.HTTPTimeoutError(title)
    elif title == "PageNotFound":
        raise wikipedia.exceptions.PageError(title)
    else:
        raise Exception("Unknown error")


@patch("wikipedia.summary", side_effect=mock_wikipedia_summary)
def test_get_wikipedia_album(mock_summary):
    # Test valid album
    response = client.get("/api/wikipedia_album/Valid Album")
    assert response.status_code == 200
    assert response.json() == {
        "summary": "This is a summary for a valid album."
    }

    # Test disambiguation error
    response = client.get("/api/wikipedia_album/Disambiguation")
    assert response.status_code == 200
    assert "DisambiguationError" in response.json()["summary"]

    # Test HTTP timeout error
    response = client.get("/api/wikipedia_album/HTTPTimeout")
    assert response.status_code == 200
    assert "HTTPTimeoutError" in response.json()["summary"]

    # Test page not found error
    response = client.get("/api/wikipedia_album/PageNotFound")
    assert response.status_code == 200
    assert "PageError" in response.json()["summary"]

    # Test unknown error
    response = client.get("/api/wikipedia_album/Invalid")
    assert response.status_code == 200
    assert "An error occurred" in response.json()["summary"]
