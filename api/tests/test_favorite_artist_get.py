from authenticator import authenticator
from main import app
from queries.favorite_artist import (
    FavoriteArtistRepository
)
from fastapi.testclient import TestClient


client = TestClient(app)


class TestFavoriteArtistRepository:
    def get_all(self, account_id):
        return []


def fake_account():
    return {"id": "1", "username": "user"}


def test_get_all_favorite_artist():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    app.dependency_overrides[
        FavoriteArtistRepository
    ] = TestFavoriteArtistRepository

    # Act
    response = client.get("/api/user/favorite_artist")

    # Clean Up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == []
