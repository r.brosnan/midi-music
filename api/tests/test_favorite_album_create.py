from authenticator import authenticator
from fastapi.testclient import TestClient
from main import app
from queries.favorite_album import (
    FavoriteAlbumOut,
    FavoriteAlbumRepository,
)

client = TestClient(app)


class TestFavoriteAlbumRepository:
    def create(
        self,
        favorite_album,
    ):
        return {
            "id": 1,
            "name": "test",
            "artist_name": "Gale Lewis",
            "release_group_mbid": "123",
            "artist_mbid": "123",
            "account_id": 1,
        }


def fake_account():
    return {"id": "1", "username": "user"}


def test_fake_create_favorite_album():
    # Arrange
    app.dependency_overrides[
        authenticator.get_current_account_data
    ] = fake_account

    app.dependency_overrides[
        FavoriteAlbumRepository
    ] = TestFavoriteAlbumRepository

    fav_album = {
        "name": "test",
        "artist_name": "Gale Lewis",
        "artist_mbid": "123",
        "release_group_mbid": "123",
        "account_id": 1,
    }

    # Act
    response = client.post("/api/favorite_album/create", json=fav_album)

    # Clean Up
    app.dependency_overrides = {}

    # Assert
    assert response.status_code == 200
    assert response.json() == FavoriteAlbumOut(
        id=1,
        name="test",
        artist_name="Gale Lewis",
        release_group_mbid="123",
        artist_mbid="123",
        account_id=1,
    )
