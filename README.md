# Midi

- Ayden Manahan
- Cayman Geiger
- Dante Burger
- Ehasan Shawan
- Riley Brosnan


## Design

- [Wire-frame](docs/wire_frame)
- [API design](docs/API/apis.md)
- [Data models](docs/data_models/midi_data_models.png)
- [Integrations](docs/Integrations/Integrations.md)
- [Issue Tracker](https://gitlab.com/groups/Fantastic-Five1/-/issues)
- Journals
    - [Ayden Manahan](journals/ayden_manahan.md)
    - [Cayman Geiger](journals/cayman_geiger.md)
    - [Dante Burger](journals/dantes_journal.md)
    - [Ehasan Shawan](journals/ehasan_s.md)
    - [Riley Brosnan](journals/riley_brosnan.md)
- Unit Tests
    - [Favorite Album Create](api/tests/test_favorite_album_create.py)
      - Tests the creation of a User's favorite Album
      - Ayden
    - [Favorite Album Get](api/tests/test_favorite_album_get.py)
      - Tests the get all of all of the User's Favorited Albums
      - Cayman
    - [Review Get](api/tests/test_review_get.py)
      - Tests the get all of all of an Album's user reviews
      - Dante
    - [Wiki Album Get](api/tests/test_wiki_album_get.py)
      - Tests the get wiki api to return an Album's wiki information
      - Ehasan
    - [Favorite Artist Create](api/tests/test_favorite_artist_create.py)
      - Tests the creation of a User's favorite Artist
      - Riley
    - [Favorite Artist Get](api/tests/test_favorite_artist_get.py)
      - Tests the get all of all of the User's Favorited Artists


## Intended market

We are targeting general music audiences in the discovery market who are looking to fill the gap of physical media that the digital market left behind. Nostalgia.


## Functionality

- Visitors to the site can explore Artist:
  - Discover Artist wiki information
  - View Albums' release dates and artwork
- Accounts
    - User can sign up for an account
    - User can securely sign in through an authenticator
- Users can favorite Artists to build a collection of Album artwork
- Users can favorite Albums for easy access
- Visitors to the site can explore Albums:
  - Discover Album wiki information
  - View artwork associated with each Album release
  - Leave Reviews of each album with a 5 star rating scale
  - View Tracks listed on each Album


## Tech Stack

- [![React][React.js]][React-url]
- [![FastAPI][FastAPI.py]][FastAPI-url]
- [![Vite][Vite.js]][Vite-url]
- [![Docker][Docker.dev]][Docker-url]
- [![Postgres][Postgres.sql]][Postgres-url]
- [![JWT][JWT.io]][JWT-url]
- [![JavaScript][JavaScript.js]][JavaScript-url]
- [![Python][Python.py]][Python-url]
- [![MusicBrainz][MusicBrainz.api]][MusicBrainz-url]
- [![Wikipedia][Wikipedia.io]][Wikipedia-url]


## Project Initialization

[Live Link](https://midi-fantastic-five1-28f57ed7ec62de863aeaf7a768417279982a32b577.gitlab.io/)

To fully enjoy this application on your local machine, please make sure to follow these steps:

1. Fork the repository
2. Clone the repository down to your local machine
3. CD into the new project directory
4. Run `git checkout dev`
5. Create .env based off of [.env_sample](.env_sample)
6. Run `docker volume create postgres-data`
7. Run `docker volume create pg-admin-data`
8. Run `docker compose build`
9. Run `docker compose up`
10. Exit the container's CLI, and enjoy


## Stretch Goals

1. Add concerts tab API for local concerts in the User's Area
2. Add Music Player through either Youtube, SoundCloud, and/or Spotify
3. Add lyrics api for each track on an Album Detail Page
4. Allow user to create an account and login on mobile device
5. Allow user to follow other users' favorites and reviews



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[React.js]: https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB
[React-url]: https://reactjs.org/
[FastAPI.py]: https://img.shields.io/badge/FastAPI-009688?style=for-the-badge&logo=FastAPI&logoColor=white
[FastAPI-url]: https://fastapi.tiangolo.com/
[Vite.js]: https://img.shields.io/badge/vite-%23646CFF.svg?style=for-the-badge&logo=vite&logoColor=white
[Vite-url]: https://vitejs.dev/
[Docker.dev]: https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white
[Docker-url]: https://www.docker.com/
[Postgres.sql]: https://img.shields.io/badge/postgres-%23316192.svg?style=for-the-badge&logo=postgresql&logoColor=white
[Postgres-url]: https://www.postgresql.org/
[MusicBrainz.api]: https://img.shields.io/badge/Musicbrainz-EB743B?style=for-the-badge&logo=musicbrainz&logoColor=BA478F
[MusicBrainz-url]: https://musicbrainz.org/doc/MusicBrainz_API
[Wikipedia.io]: https://img.shields.io/badge/Wikipedia-%23000000.svg?style=for-the-badge&logo=wikipedia&logoColor=white
[Wikipedia-url]: https://pypi.org/project/wikipedia/#description
[JavaScript.js]: https://img.shields.io/badge/javascript-%23323330.svg?style=for-the-badge&logo=javascript&logoColor=%23F7DF1E
[JavaScript-url]: https://developer.mozilla.org/en-US/docs/Web/JavaScript
[Python.py]: https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54
[Python-url]: https://www.python.org/
[JWT.io]: https://img.shields.io/badge/JWT-black?style=for-the-badge&logo=JSON%20web%20tokens
[JWT-url]: https://jwt.io/
